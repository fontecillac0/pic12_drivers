
/* 
 * File: ADC header    
 * Author: David Fontecilla Carrillo
 * Comments: Includes a initial configuration function and other functions
 *           facilitate the A/D conversion of the PIC12F675
 * Revision history: 
 */


#ifndef XC_HEADER_TEMPLATE_H
#define	XC_HEADER_TEMPLATE_H
#include <xc.h> // include processor files - each processor file is guarded. 
#include <stdio.h>
#include <stdint.h>
void set_adc(uint8_t gpio);
int read_value(uint8_t channel);
#ifdef __cplusplus
#endif	/* XC_HEADER_TEMPLATE_H */
#ifdef	__cplusplus
#endif
#endif
