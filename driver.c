/*
 * File:   adc_driver.c
 * Author: David Fontecilla
 *
 * Created on December 20 2020
 * 
 * A/D library functions 
 */

#include <xc.h>
#include <stdint.h>
#include "pic12_adc.h"
/*all the initial configuration required of the A/D*/
void set_adc(uint8_t gpio)
{
    ADCON0bits.ADON = 0;//turn off the A/D momentaniously
    
    /*routine that will shift the MSB of the GPIO 4 bits
     because the analogue ports are on GP0,GP1,GP2 and GP4
     and the gpio bits are from GP0 to GP3*/
    if(gpio & 0x08)
    {
        TRISIO = TRISIO | 0x10;
        TRISIO = TRISIO | (gpio & 0b00000111);
    }//set the A/D port as input
    ANSELbits.ANS = gpio;//analog inputs
    ADCON0bits.VCFG = 0; //vdd as reference
    ANSELbits.ADCS = 0b001;//highest speed recommended by the datasheet using an internal oscillator
    ADCON0bits.GO_DONE = 0;//do not start conversion yet
    ADCON0bits.ADFM = 1; //the LSB will be in ADRESL and the 2 MSB will be in ADRESH
}

/*function that returns the final value of the conversion
 according to the input of the selected channel*/
int read_value(uint8_t chann)
{
    ADCON0bits.ADON = 1;//turn on the A/D
    ADCON0bits.CHS = chann;//2 bits to select from channel 0 to 3
    ADCON0bits.GO_DONE = 1;//start conversion
    while(ADCON0bits.GO_DONE == 1);//wait until the A/D flag is 0
    int conversion = ADRESL + (ADRESH << 8);//read the registers were the conversion was stored
    return conversion;//return the conversion result in bits
}