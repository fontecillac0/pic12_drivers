/*
 * File:   main_example.c
 * Author: David Fontecilla
 *
 * Created on December 21 2020
 * 
 * Example of the PIC12F675 A/D library with a 
 * LM35 temperature sensor 
 * 
 * the sensor signal was 10 times amplified in order 
 * to read a wider range of bits 
 */

#include <xc.h>
//using the internal oscillator
#pragma config FOSC=INTRCIO,WDTE=OFF,MCLRE=OFF,BOREN=OFF
#include <stdio.h>
#include <stdlib.h>
#include "pic12_adc.h"
//variable which will store our conversion in bits (10 bit A/D)
int conversion;
void main(void) {
    TRISIObits.TRISIO2 = 0;
    TRISIObits.TRISIO4 = 0;
    TRISIObits.TRISIO5 = 0;
    //set_adc parameter is the analogue ports that we want to use
    set_adc(0b0011);
    while(1)
    {
        GPIO = 0x00;
        /*read_value parameter is the channel we want to read from
         * the function returns the conversion in bits*/  
        conversion = read_value(0b00);
        if((conversion > 600)&&(conversion < 650))
        {
            GP5 = 1;
            GP4 = 0;
            GP2 = 0;
        }
        else if((conversion >650)&&(conversion<700))
        {
            GP5 = 0;
            GP4 = 1;
            GP2 = 0;
        }
        else if(conversion > 700)
        {
            GP2 = 1;
            GP4 = 0;
            GP5 = 0;
        }
    }
    
}
